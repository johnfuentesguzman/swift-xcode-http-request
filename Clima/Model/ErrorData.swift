//
//  ErrorModel.swift
//  Clima
//
//  Created by John Fuentes on 22/03/20.
//  Copyright © 2020 App Brewery. All rights reserved.
//

//
//  WeatherModel.swift
//  Clima
//

import Foundation

struct ErrorData: Codable {
    let cod: String
    let message: String
}
