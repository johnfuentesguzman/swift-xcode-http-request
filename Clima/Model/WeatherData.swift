//
//  WeatherData.swift
//  Clima
//

import Foundation

struct WeatherData: Codable {
    let name: String
    let main: Main // main struct below
    let weather: [Weather] // Weather struct below
}

struct Main: Codable {
    let temp: Double
}

struct Weather: Codable {
    let description: String
    let id: Int
}
