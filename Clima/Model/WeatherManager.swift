//
//  WeatherManager.swift
//  Clima
//

import Foundation
import CoreLocation

protocol WeatherManagerDelegate {
    func didUpdateWeather(_ weatherManager: WeatherManager, weather: WeatherModel)
    func didFailWithError(_ error: String)
}

struct WeatherManager {
    let weatherURL = "https://api.openweathermap.org/data/2.5/weather?appid=fe95b7fe9c1e43d8b68e5bf4f59bfead&units=metric"
    
    var delegate: WeatherManagerDelegate? // this is the protocol used in this struct
     
    func fetchWeather(cityName: String ) {
        let urlString = "\(weatherURL)&q=\(cityName)"
        performRequest(with: urlString)
    }
    
    func fetchWeather(latitude: CLLocationDegrees, longitude: CLLocationDegrees) {
        let urlString = "\(weatherURL)&lat=\(latitude)&lon=\(longitude)"
        performRequest(with: urlString)
    }
    
    func performRequest(with urlString: String) {
        if let url = URL(string: urlString) {
            let session = URLSession(configuration: .default) // It Prepares the tasks to be executed in the browser  (download / fetch in this case)
            let task = session.dataTask(with: url) { (data, response, error) in // It returns a clousured function (in)  with the object with data error or success
                if let error = data {
                    if let errorLog = self.parseErrorJson(error){
                       self.delegate?.didFailWithError(errorLog)
                    }
                }
                
                if let safeData = data {
                    if let weather = self.parseJSON(safeData) {
                        self.delegate?.didUpdateWeather(self, weather: weather) // callng the protocol.. BTW this function is in wheaterVievController
                    }
                }
            }
            task.resume()
        }
    }
    
    func parseJSON(_ weatherData: Data) -> WeatherModel? {
        let decoder = JSONDecoder()
        do {
            let decodedData = try decoder.decode(WeatherData.self, from: weatherData)
            let id = decodedData.weather[0].id
            let desc = decodedData.weather[0].description
            let temp = decodedData.main.temp
            let name = decodedData.name
            
            let weather = WeatherModel(conditionId: id, cityName: name, temperature: temp, description: desc)
            return weather
            
        } catch {
//           // delegate?.didFailWithError(error: error)
            return nil
        }
    }
    
    func parseErrorJson(_ error: Data) -> String? {
        let decoder = JSONDecoder()
        do {
            let decodeData = try decoder.decode(ErrorData.self, from: error)
            return decodeData.message;

        } catch {
            return ""
        }
    }
    
    
    
}


